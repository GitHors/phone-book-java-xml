/**
 *
 */
package by.iba.gomel.phonebook.tests;

/**
 *
 */
public class TestConstants {
	public static final String MAIN_MENU_HEADER = "1. Displays a list of phone numbers\n" + "2. Add new phone number\n"
			+ "3. Change some item\n" + "4. Remove some item\n" + "5. Search item\n" + "6. Plug xml file\n"
			+ "7. Exit\n\n";
}

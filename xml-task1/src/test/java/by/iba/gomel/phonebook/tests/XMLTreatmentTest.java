/**
 *
 */
package by.iba.gomel.phonebook.tests;

import org.junit.Assert;
import org.junit.Test;

import by.iba.gomel.phonebook.XMLTreatment;

/**
 * Tests for class {@link XMLTreatment}.
 */
public class XMLTreatmentTest {

	/**
	 * Test method for
	 * {@link by.iba.gomel.phonebook.XMLTreatment#isExternalDTDValid(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testInvalidWithDTDFile() {
		final ClassLoader classLoader = this.getClass().getClassLoader();
		final String xmlFileName = classLoader.getResource("invalid_file.xml").getPath();
		final String dtdFileName = classLoader.getResource("test.dtd").getPath();
		final boolean expectedValidationResult = false;
		Assert.assertEquals("", expectedValidationResult, XMLTreatment.isExternalDTDValid(xmlFileName, dtdFileName));
	}

	/**
	 * Test method for
	 * {@link by.iba.gomel.phonebook.XMLTreatment#isXMLFileFormat(java.lang.String)}.
	 */
	@Test
	public void testRightXMLFileFormat() {

		final String xmlFileName = "test.xml";
		final boolean expectedResult = true;
		Assert.assertEquals("", expectedResult, XMLTreatment.isXMLFileFormat(xmlFileName));
	}

	/**
	 * Test method for
	 * {@link by.iba.gomel.phonebook.XMLTreatment#isExternalDTDValid(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testValidWithDTDFile() {
		final ClassLoader classLoader = this.getClass().getClassLoader();
		final String xmlFileName = classLoader.getResource("valid_file.xml").getPath();
		final String dtdFileName = classLoader.getResource("test.dtd").getPath();
		final boolean expectedValidationResult = true;
		Assert.assertEquals("", expectedValidationResult, XMLTreatment.isExternalDTDValid(xmlFileName, dtdFileName));
	}

	/**
	 * Test method for
	 * {@link by.iba.gomel.phonebook.XMLTreatment#isXMLFileFormat(java.lang.String)}.
	 */
	@Test
	public void testWrongXMLFileFormat() {
		final String xmlFileName = "test.xxml";
		final boolean expectedResult = false;
		Assert.assertEquals("", expectedResult, XMLTreatment.isXMLFileFormat(xmlFileName));
	}

}

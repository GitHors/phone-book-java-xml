/**
 *
 */
package by.iba.gomel.phonebook.models.tests;

import org.junit.Assert;
import org.junit.Test;

import by.iba.gomel.phonebook.models.PhoneBookItem;

/**
 * Tests for class {@link PhoneBookItem}.
 */
public class PhoneBookItemTest {

	PhoneBookItem phoneBookItem = new PhoneBookItem("Denis", "Pryshchcep", "Test adress", "+375291769450");

	/**
	 * Test method for
	 * {@link by.iba.gomel.phonebook.models.PhoneBookItem#getItemInfo()}.
	 */
	@Test
	public void testGetItemInfo() {
		final String expectedPhoneBookItemInfo = "Name: Denis      Lastname: Pryshchcep Adress: Test adress     Number: +375291769450       ";
		Assert.assertEquals("Error in getting info about phone book item", expectedPhoneBookItemInfo,
				this.phoneBookItem.getItemInfo());
	}

	/**
	 * Test method for
	 * {@link by.iba.gomel.phonebook.models.PhoneBookItem#getItemInfo()}.
	 */
	@Test
	public void testGetXMLFormatView() {
		final PhoneBookItem phoneBookItem = new PhoneBookItem("test name", "test last name", "test adress", "000000");
		final String expectedResult = "<item starred=\"no\">\n" + "	<name>test name</name>\n"
				+ "	<lastname>test last name</lastname>\n" + "	<adress>test adress</adress>\n"
				+ "	<number is_viber=\"yes\">000000</number>\n" + "</item>";
		Assert.assertEquals("Error in comparing objcts", expectedResult, phoneBookItem.getXMLFormatView());
	}

	/**
	 * Test method for
	 * {@link by.iba.gomel.phonebook.models.PhoneBookItem#getItemInfo()}.
	 */
	@Test
	public void testRightEqualsItems() {
		final PhoneBookItem phoneBookItem = new PhoneBookItem("test name", "test last name", "test adress", "000000");
		final PhoneBookItem samePhoneBookItem = new PhoneBookItem("test name", "test last name", "test adress",
				"000000");
		final boolean expectedResult = true;
		Assert.assertEquals("Error in comparing objcts", expectedResult, phoneBookItem.equals(samePhoneBookItem));
	}

	/**
	 * Test method for
	 * {@link by.iba.gomel.phonebook.models.PhoneBookItem#getItemInfo()}.
	 */
	@Test
	public void testWrongEqualsItems() {
		final PhoneBookItem phoneBookItem = new PhoneBookItem("test name", "test last name", "test adress", "000000");
		final PhoneBookItem samePhoneBookItem = new PhoneBookItem("fail name", "fail last name", "test adress",
				"000000");
		final boolean expectedResult = false;
		Assert.assertEquals("Error in comparing objcts", expectedResult, phoneBookItem.equals(samePhoneBookItem));
	}

}

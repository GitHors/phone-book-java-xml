/**
 *
 */
package by.iba.gomel.phonebook.console.tests;

import java.io.ByteArrayInputStream;

import org.junit.Assert;
import org.junit.Test;

import by.iba.gomel.phonebook.console.ConsoleOperations;

/**
 * Tests for class {@link ConsoleOperations}.
 */
public class ConsoleOperationTest {

	/**
	 * Test method for {@link by.iba.gomel.phonebook.console.gomel.iba.ConsoleOperations#scanInfo()}.
	 */
	@Test
	public void testScanInfo() {
		final ByteArrayInputStream in = new ByteArrayInputStream("test".getBytes());
		System.setIn(in);
		final String expectedResult = "test";
		Assert.assertEquals("Error in read information from console", expectedResult, ConsoleOperations.readInfo());
	}
}

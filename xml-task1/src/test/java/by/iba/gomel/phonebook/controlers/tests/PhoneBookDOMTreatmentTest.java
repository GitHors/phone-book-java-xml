/**
 *
 */
package by.iba.gomel.phonebook.controlers.tests;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

import by.iba.gomel.phonebook.controlers.PhoneBookDOMTreatment;
import by.iba.gomel.phonebook.models.PhoneBookItem;

/**
 *
 */
public class PhoneBookDOMTreatmentTest {

	@Rule
	public final SystemOutRule log = new SystemOutRule().enableLog();

	/**
	 * Test method for
	 * {@link by.iba.gomel.phonebook.controlers.PhoneBookDOMTreatment#addPhoneItem(java.lang.String, java.lang.String, java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testAddEmptyPhoneItem() {
		final PhoneBookDOMTreatment treatment = new PhoneBookDOMTreatment();

		// Add new item
		treatment.addPhoneItem("", "", "", "");

		final int itemsAmount = treatment.getAmountOfItems();

		// Create object of item which we added
		final PhoneBookItem itemForTest = treatment.getPhoneItemValue(itemsAmount);
		final PhoneBookItem itemForCompare = new PhoneBookItem("", "", "", "");

		// Remove element which we used for testing
		treatment.removePhoneItem(itemsAmount);

		Assert.assertTrue("Error in add phone book item (with empty values) action",
				itemForTest.equals(itemForCompare));
	}

	/**
	 * Test method for
	 * {@link by.iba.gomel.phonebook.controlers.PhoneBookDOMTreatment#addPhoneItem(java.lang.String, java.lang.String, java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testAddPhoneItem() {
		final PhoneBookDOMTreatment treatment = new PhoneBookDOMTreatment();
		final int previousItemsAmount = treatment.getAmountOfItems();

		// Add new item
		treatment.addPhoneItem("test name", "test last name", "test adress", "");
		final int currentItemsAmount = treatment.getAmountOfItems();

		// Remove element which we used for testing
		treatment.removePhoneItem(currentItemsAmount);

		Assert.assertEquals("Error in add phone book item action", previousItemsAmount + 1, currentItemsAmount);
	}

	/**
	 * Test method for
	 * {@link by.iba.gomel.phonebook.controlers.PhoneBookDOMTreatment#addPhoneItem(java.lang.String, java.lang.String, java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testAddPhoneItemWithEquals() {
		final PhoneBookDOMTreatment treatment = new PhoneBookDOMTreatment();

		// Add new item
		treatment.addPhoneItem("test name", "test last name", "test adress", "");
		final int itemsAmount = treatment.getAmountOfItems();

		// Create object of item which we added
		final PhoneBookItem itemForTest = treatment.getPhoneItemValue(itemsAmount);
		final PhoneBookItem itemForCompare = new PhoneBookItem("test name", "test last name", "test adress", "");

		// Remove element which we used for testing
		treatment.removePhoneItem(itemsAmount);

		Assert.assertTrue("Error in add phone book item action", itemForTest.equals(itemForCompare));
	}

	/**
	 * Test method for
	 * {@link by.iba.gomel.phonebook.controlers.PhoneBookDOMTreatment#changeItemState(int, by.iba.gomel.phonebook.models.PhoneBookItem)}.
	 */
	@Test
	public void testChangeItemState() {
		final PhoneBookDOMTreatment treatment = new PhoneBookDOMTreatment();
		final int itemsAmount = treatment.getAmountOfItems();

		final PhoneBookItem itemForTest = treatment.getPhoneItemValue(itemsAmount);
		final PhoneBookItem itemForCompare = new PhoneBookItem("test name", "test last name", "test adress", "");

		treatment.changeItemState(itemsAmount, itemForTest);

		Assert.assertFalse("Error in change phone book item action", itemForTest.equals(itemForCompare));

	}

	/**
	 * Test method for
	 * {@link by.iba.gomel.phonebook.controlers.PhoneBookDOMTreatment#getPhoneItemValue(int)}.
	 */
	@Test
	public void testGetPhoneItemValue() {
		final PhoneBookDOMTreatment treatment = new PhoneBookDOMTreatment();

		// Add new item
		treatment.addPhoneItem("test name", "test last name", "test adress", "");
		final int itemsAmount = treatment.getAmountOfItems();

		// Create object of item which we added
		final PhoneBookItem itemForTest = treatment.getPhoneItemValue(itemsAmount);
		final PhoneBookItem itemForCompare = new PhoneBookItem("test name", "test last name", "test adress", "");

		// Remove element which we used for testing
		treatment.removePhoneItem(itemsAmount);

		Assert.assertTrue("Error in get phone book item action", itemForTest.equals(itemForCompare));
	}

}

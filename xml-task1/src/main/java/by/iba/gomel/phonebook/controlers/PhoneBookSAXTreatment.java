package by.iba.gomel.phonebook.controlers;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import by.iba.gomel.phonebook.console.ConsoleOperations;
import by.iba.gomel.phonebook.handlers.sax.FullPhoneBookHandler;
import by.iba.gomel.phonebook.handlers.sax.SearchHandler;
import by.iba.gomel.phonebook.models.PhoneBookItem;

public class PhoneBookSAXTreatment implements PhoneBookTreatment {

	/**
	 * Logger.
	 *
	 * For printing information we used Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(PhoneBookSAXTreatment.class);

	private File xmlFile;
	private final String xsdFilePath;
	SAXParserFactory saxFactory;
	SAXParser saxParser;

	/**
	 * Constructor.
	 */
	public PhoneBookSAXTreatment() {
		this.setSourceFilePath(Constants.DEFAULT_XML_PHONE_BOOK_FILE_PATH);
		// Load file path of default xml sources
		final ClassLoader classLoader = this.getClass().getClassLoader();
		this.xmlFile = new File(classLoader.getResource(Constants.DEFAULT_XML_PHONE_BOOK_FILE_PATH).getFile());
		// Load file path of default dtd sources
		this.xsdFilePath = classLoader.getResource(Constants.DEFAULT_XSD_PHONE_BOOK_FILE_PATH).getPath();

		this.saxFactory = SAXParserFactory.newInstance();
		try {
			this.saxParser = this.saxFactory.newSAXParser();
		} catch (ParserConfigurationException | SAXException e) {
			PhoneBookSAXTreatment.LOGGER.error("", e);
		}
	}

	/**
	 * Method for adding item in phone book (xml file).
	 *
	 * @param name
	 *            String value.
	 * @param lastname
	 *            String value.
	 * @param adress
	 *            String value.
	 * @param number
	 *            String value.
	 */
	@Override
	public void addPhoneItem(final String name, final String lastname, final String adress, final String number) {
		try {
			final FullPhoneBookHandler itemHandler = new FullPhoneBookHandler();
			this.saxParser.parse(this.xmlFile, itemHandler);
			final List<PhoneBookItem> items = itemHandler.getAllItems();
			items.add(new PhoneBookItem(name, lastname, adress, number));
			this.saveFileState(items);
		} catch (final IOException | SAXException e) {
			PhoneBookSAXTreatment.LOGGER.error("", e);
		}

		ConsoleOperations.printInfo(Constants.NEW_LINE);
	}

	@Override
	public void changeItemState(final int index, final PhoneBookItem item) {
		if (index > 0) {
			List<PhoneBookItem> items = new ArrayList<>();
			try {
				final FullPhoneBookHandler itemHandler = new FullPhoneBookHandler();
				this.saxParser.parse(this.xmlFile, itemHandler);
				items = itemHandler.getAllItems();
			} catch (final IOException | SAXException e) {
				PhoneBookSAXTreatment.LOGGER.error("", e);
			}

			if (index <= items.size()) {
				items.set(index - 1, item);
				this.saveFileState(items);
			} else {
				ConsoleOperations.printInfo(Constants.PHONE_BOOK_ITEM_INDEX_ERROR_MESSAGE + Constants.NEW_LINE);
			}
		} else {
			ConsoleOperations.printInfo(Constants.PHONE_BOOK_NEGATIVE_ITEM_INDEX_ERROR_MESSAGE + Constants.NEW_LINE);
		}

	}

	/**
	 * Method used for display all information from xml file using sax parser.
	 */
	@Override
	public void getAllPhoneItems() {
		try {
			final FullPhoneBookHandler itemHandler = new FullPhoneBookHandler();
			this.saxParser.parse(this.xmlFile, itemHandler);
			final List<PhoneBookItem> items = itemHandler.getAllItems();
			for (int i = 0; i < items.size(); i++) {
				ConsoleOperations.printInfo("№ " + (i + 1) + items.get(i).getItemInfo());
			}
		} catch (final IOException | SAXException e) {
			PhoneBookSAXTreatment.LOGGER.error("", e);
		}

		ConsoleOperations.printInfo(Constants.NEW_LINE);
	}

	/**
	 * Method for getting amount of phone book items in items in xml file.
	 *
	 * @return
	 */
	@Override
	public int getAmountOfItems() {
		return 0;
	}

	/**
	 * Method for getting info about some phone book item by index. We use sax
	 * parser in this method.
	 *
	 * @param index
	 *            index of item.
	 * @return PhoneBookItem object with all info about item.
	 */
	@Override
	public PhoneBookItem getPhoneItemValue(final int index) {
		if (index > 0) {
			List<PhoneBookItem> items = new ArrayList<>();
			try {
				final FullPhoneBookHandler itemHandler = new FullPhoneBookHandler();
				this.saxParser.parse(this.xmlFile, itemHandler);
				items = itemHandler.getAllItems();
			} catch (final IOException | SAXException e) {
				PhoneBookSAXTreatment.LOGGER.error("", e);
			}

			if (index <= items.size()) {
				return items.get(index - 1);
			} else {
				ConsoleOperations.printInfo(Constants.PHONE_BOOK_ITEM_INDEX_ERROR_MESSAGE + Constants.NEW_LINE);
			}
		} else {
			ConsoleOperations.printInfo(Constants.PHONE_BOOK_NEGATIVE_ITEM_INDEX_ERROR_MESSAGE + Constants.NEW_LINE);
		}
		return null;
	}

	@Override
	public String getValidatorFilePath() {
		return this.getXsdFilePath();
	}

	/**
	 * Method for getting current dtd file path.
	 *
	 * @return String value.
	 */
	public String getXsdFilePath() {
		return this.xsdFilePath;
	}

	/**
	 * Method for removing "item" from xml file.
	 *
	 * @param index
	 *            index of item that we wants to delete.
	 */
	@Override
	public void removePhoneItem(final int index) {
		if (index > 0) {
			List<PhoneBookItem> items = new ArrayList<>();
			try {
				final FullPhoneBookHandler itemHandler = new FullPhoneBookHandler();
				this.saxParser.parse(this.xmlFile, itemHandler);
				items = itemHandler.getAllItems();
			} catch (final IOException | SAXException e) {
				PhoneBookSAXTreatment.LOGGER.error("", e);
			}

			if (index <= items.size()) {
				items.remove(index - 1);
				this.saveFileState(items);
			} else {
				ConsoleOperations.printInfo(Constants.PHONE_BOOK_ITEM_INDEX_ERROR_MESSAGE + Constants.NEW_LINE);
			}
		} else {
			ConsoleOperations.printInfo(Constants.PHONE_BOOK_NEGATIVE_ITEM_INDEX_ERROR_MESSAGE + Constants.NEW_LINE);
		}
	}

	public void saveFileState(final List<PhoneBookItem> items) {
		try (final PrintWriter writer = new PrintWriter(this.xmlFile)) {
			writer.println("<items>");
			for (final PhoneBookItem item : items) {
				writer.println(item.getXMLFormatView());
			}
			writer.println("</items>");
		} catch (final IOException e) {
			PhoneBookSAXTreatment.LOGGER.error("", e);
		}
	}

	/**
	 * Method for search item by some key(name, lastname and ect.) and value. We
	 * use sax parser in this case.
	 *
	 * @param searchValue
	 *            String value.
	 */
	@Override
	public void searchPhoneItem(final String searchValue) {
		try {
			final SearchHandler itemHandler = new SearchHandler(searchValue);
			this.saxParser.parse(this.xmlFile, itemHandler);
			final List<PhoneBookItem> items = itemHandler.getAllItems();
			items.forEach(e -> ConsoleOperations.printInfo(e.getItemInfo()));
		} catch (final IOException | SAXException e) {
			PhoneBookSAXTreatment.LOGGER.error("", e);
		}
	}

	@Override
	public void setSourceFilePath(final String filePath) {
		this.xmlFile = new File(filePath);
	}

}

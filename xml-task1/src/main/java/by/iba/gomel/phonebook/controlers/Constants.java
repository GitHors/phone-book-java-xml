/**
 *
 */
package by.iba.gomel.phonebook.controlers;

/**
 *
 */
public final class Constants {

	// File paths for main application files with information
	public static final String DEFAULT_XML_PHONE_BOOK_FILE_PATH = "info/phone_items.xml";
	public static final String DEFAULT_DTD_PHONE_BOOK_FILE_PATH = "info/phone_items.dtd";
	public static final String DEFAULT_XSD_PHONE_BOOK_FILE_PATH = "info/phone_items.xsd";

	// Some error messages for user
	public static final String PHONE_BOOK_ITEM_INDEX_ERROR_MESSAGE = "There is no such item in our phone book";
	public static final String PHONE_BOOK_NEGATIVE_ITEM_INDEX_ERROR_MESSAGE = "Number must be more then 0";

	public static final String NEW_LINE = "\n";

	// Names of tags in xml file
	public static final String TAG_NAME = "name";
	public static final String TAG_LASTNAME = "lastname";
	public static final String TAG_ADRESS = "adress";
	public static final String TAG_PHONE_NUMBER = "number";
	public static final String TAG_ITEM = "item";
	public static final String TAG_ITEMS = "items";

	/**
	 * Constructor.
	 */
	Constants() {
		// Empty constructor.
	}
}

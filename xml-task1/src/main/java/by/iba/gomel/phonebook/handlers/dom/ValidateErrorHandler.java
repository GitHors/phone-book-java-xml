/**
 *
 */
package by.iba.gomel.phonebook.handlers.dom;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * Handler for validate xml document.
 */
public class ValidateErrorHandler implements ErrorHandler {

	// Flag for error existing
	private boolean isAnyErrorExist = false;

	@Override
	public void error(final SAXParseException exception) throws SAXException {
		this.isAnyErrorExist = true;
	}

	@Override
	public void fatalError(final SAXParseException exception) throws SAXException {
		this.isAnyErrorExist = true;
	}

	/**
	 * Method used for getting file validation result.
	 *
	 * @return boolean value (false - if file contain one or more error or
	 *         warning).
	 */
	public boolean isFileValid() {
		return !this.isAnyErrorExist;
	}

	@Override
	public void warning(final SAXParseException exception) throws SAXException {
		this.isAnyErrorExist = true;
	}
}

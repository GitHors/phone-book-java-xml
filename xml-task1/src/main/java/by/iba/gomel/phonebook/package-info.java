/**
 * Package contains some classes which provide application for phone book
 * treatment (add, read some info). Information about phone book stores in xml
 * files which located in src/main/resources directory.
 */
package by.iba.gomel.phonebook;
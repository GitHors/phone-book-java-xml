/**
 *
 */
package by.iba.gomel.phonebook.models;

import java.security.SecureRandom;
import java.util.Formatter;
import java.util.Locale;

/**
 * Class used for provide work with one item from phone book. After parsing
 * information from xml file we can create instance of this item for convenient
 * treatment in future.
 */
public final class PhoneBookItem {

	private String name;
	private String lastname;
	private String adress;
	private String number;

	/**
	 * Default constructor.
	 */
	public PhoneBookItem() {
		// Empty constructor.
	}

	/**
	 * Constructor.
	 */
	public PhoneBookItem(final String name, final String lastname, final String adress, final String number) {
		this.setName(name);
		this.setLastname(lastname);
		this.setAdress(adress);
		this.setNumber(number);
	}

	@Override
	public boolean equals(final Object obj) {
		if ((obj instanceof PhoneBookItem) && ((PhoneBookItem) obj).getName().equals(this.name)
				&& ((PhoneBookItem) obj).getLastname().equals(this.lastname)
				&& ((PhoneBookItem) obj).getNumber().equals(this.number)) {
			return true;
		}
		return false;
	}

	/**
	 * Method for getting adress.
	 *
	 * @return String value.
	 */
	public String getAdress() {
		return this.adress;
	}

	/**
	 * Method for getting information info about phone book item.
	 *
	 * @return String value.
	 */
	public String getItemInfo() {
		final StringBuilder sb = new StringBuilder();
		final Formatter formatter = new Formatter(sb, Locale.US);
		final String result = formatter.format("Name: %-10s Lastname: %-10s Adress: %-15s Number: %-20s", this.name,
				this.lastname, this.adress, this.number).toString();
		formatter.close();
		return result;
	}

	/**
	 * Method for get lastname from phone book item.
	 *
	 * @return String value.
	 */
	public String getLastname() {
		return this.lastname;
	}

	/**
	 * Method for get nsme from phone book item.
	 *
	 * @return String value.
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Method for get number from phone book item.
	 *
	 * @return String value.
	 */
	public String getNumber() {
		return this.number;
	}

	/**
	 *
	 * @return
	 */
	public String getXMLFormatView() {
		return String.format(
				"<item starred=\"no\">%n	<name>%s</name>%n	"
						+ "<lastname>%s</lastname>%n	<adress>%s</adress>%n	<number is_viber=\"yes\">%s</number>%n</item>",
				this.name, this.lastname, this.adress, this.number);
	}

	@Override
	public int hashCode() {
		final SecureRandom secureRandom = new SecureRandom();
		return 127 * (secureRandom.nextInt(this.name.length()));
	}

	public boolean isInfoExistInItem(final String info) {
		return this.name.equals(info) || this.lastname.equals(info) || this.adress.equals(info)
				|| this.number.equals(info);
	}

	/**
	 * Method for set adress at phone book item.
	 *
	 * @param adress
	 *            String value.
	 */
	public void setAdress(final String adress) {
		this.adress = adress;
	}

	/**
	 * Method for set last at phone book item.
	 *
	 * @param lastname
	 *            String value.
	 */
	public void setLastname(final String lastname) {
		this.lastname = lastname;
	}

	/**
	 * Method for set name at phone book item.
	 *
	 * @param name
	 *            String value.
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * Method for set number at phone book item.
	 *
	 * @param number
	 *            String value.
	 */
	public void setNumber(final String number) {
		this.number = number;
	}

}

/**
 *
 */
package by.iba.gomel.phonebook.handlers.sax;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import by.iba.gomel.phonebook.controlers.Constants;
import by.iba.gomel.phonebook.models.PhoneBookItem;

/**
 * Class for reading info from xml tags.
 */
public final class SearchHandler extends DefaultHandler {

	boolean isName = false;
	boolean isLastName = false;
	boolean isAdress = false;
	boolean isPhoneNumber = false;
	List<PhoneBookItem> items = new ArrayList<>();

	String searchKey;

	PhoneBookItem phoneBookItem;

	/**
	 * Constructor.
	 */
	public SearchHandler(final String searchKey) {
		this.searchKey = searchKey;
		this.phoneBookItem = new PhoneBookItem();
	}

	@Override
	public void characters(final char[] ch, final int start, final int length) throws SAXException {
		if (this.isName) {
			this.phoneBookItem.setName(new String(ch, start, length));
			this.isName = false;
		} else if (this.isLastName) {
			this.phoneBookItem.setLastname(new String(ch, start, length));
			this.isLastName = false;
		} else if (this.isAdress) {
			this.phoneBookItem.setAdress(new String(ch, start, length));
			this.isAdress = false;
		} else if (this.isPhoneNumber) {
			this.phoneBookItem.setNumber(new String(ch, start, length));
			this.isPhoneNumber = false;
		}
	}

	@Override
	public void endElement(final String uri, final String localName, final String qName) throws SAXException {
		if (qName.equalsIgnoreCase(Constants.TAG_ITEM) && this.phoneBookItem.isInfoExistInItem(this.searchKey)) {
			this.items.add(this.phoneBookItem);
		}
	}

	public List<PhoneBookItem> getAllItems() {
		return this.items;
	}

	@Override
	public void startElement(final String uri, final String localName, final String qName, final Attributes attributes)
			throws SAXException {
		if (qName.equalsIgnoreCase(Constants.TAG_NAME)) {
			this.isName = true;
		} else if (qName.equalsIgnoreCase(Constants.TAG_LASTNAME)) {
			this.isLastName = true;
		} else if (qName.equalsIgnoreCase(Constants.TAG_ADRESS)) {
			this.isAdress = true;
		} else if (qName.equalsIgnoreCase(Constants.TAG_PHONE_NUMBER)) {
			this.isPhoneNumber = true;
		} else if (qName.equalsIgnoreCase(Constants.TAG_ITEM)) {
			this.phoneBookItem = new PhoneBookItem();
		}
	}

}

/**
 *
 */
package by.iba.gomel.phonebook;

import java.io.File;

import by.iba.gomel.phonebook.console.ConsoleOperations;
import by.iba.gomel.phonebook.controlers.PhoneBookDOMTreatment;
import by.iba.gomel.phonebook.controlers.PhoneBookSAXTreatment;
import by.iba.gomel.phonebook.controlers.PhoneBookTreatment;
import by.iba.gomel.phonebook.models.PhoneBookItem;

/**
 * Class used for organize conversation with user. Program offer for user some
 * action (show, change, remove, find information from phone book).
 */
public final class Conversation {

	// Values for menu treatment in switch/case block
	public static final int DISPLAY_ITEMS_MENU_VALUE = 1;
	public static final int ADD_ITEM_MENU_VALUE = 2;
	public static final int CHANGE_ITEM_MENU_VALUE = 3;
	public static final int REMOVE_ITEM_MENU_VALUE = 4;
	public static final int SEARCH_ITEM_MENU_VALUE = 5;
	public static final int PLUG_XML_FILE_MENU_VALUE = 6;
	public static final int CHANGE_PARSER_MENU_VALUE = 7;
	public static final int EXIT_ITEM_MENU_VALUE = 8;

	// Messages for main application menu
	public static final String DISPLAY_ITEMS_MENU_MESSAGE = "1. Displays a list of phone numbers\n";
	public static final String ADD_ITEM_MENU_MESSAGE = "2. Add new phone number\n";
	public static final String CHANGE_ITEM_MENU_MESSAGE = "3. Change some item\n";
	public static final String REMOVE_ITEM_MENU_MESSAGE = "4. Remove some item\n";
	public static final String SEARCH_ITEM_MENU_MESSAGE = "5. Search item\n";
	public static final String PLUG_XML_FILE_MENU_MESSAGE = "6. Plug xml file\n";
	public static final String CHANGE_PARSER_MENU_MESSAGE = "7. Change parser\n";
	public static final String EXIT_ITEM_MENU_MESSAGE = "8. Exit\n";

	// Key for treatment save changes action
	private static final int CLOSE_CHANGES_DIALOG_KEY = 5;

	// Messages for change values
	public static final String ITEM_PUT_NAME_MESSAGE = "Put name:";
	public static final String ITEM_PUT_LASTNAME_MESSAGE = "Put lastname:";
	public static final String ITEM_PUT_ADRESS_MESSAGE = "Put adress:";
	public static final String ITEM_PUT_NUMBER_MESSAGE = "Put number:";

	// Values for "change info" menu treatment in switch/case block
	private static final int CHANGE_ITEM_NAME_VALUE = 1;
	private static final int CHANGE_ITEM_LASTNAME_VALUE = 2;
	private static final int CHANGE_ITEM_ADRESS_VALUE = 3;
	private static final int CHANGE_ITEM_NUMBER_VALUE = 4;
	private static final int CHANGE_ITEM_FINISH_VALUE = 5;
	private static final int RETURN_TO_MAIN_MENU_VALUE = 6;

	// Messages for "change info" action menu
	private static final String CHANGE_ITEM_NAME_MESSAGE = "1. Change name: \n";
	private static final String CHANGE_ITEM_LASTNAME_MESSAGE = "2. Change lastname: \n";
	private static final String CHANGE_ITEM_ADRESS_MESSAGE = "3. Change adress: \n";
	private static final String CHANGE_ITEM_NUMBER_MESSAGE = "4. Change number: \n";
	private static final String CHANGE_ITEM_FINISH_EDITING = "5. Save changes and exit\n";
	private static final String RETURN_TO_MAIN_MENU = "6. Return to main menu";

	// Other application strings
	public static final String CONFIRM_ACTION_MESSAGE = "Do you really want delete this item (y/n)?";
	private static final String REMOVE_ITEM_MESSAGE = "Put index of item which you want to remove: ";
	private static final String REMOVE_EXIT_MESSAGE = "You are close deleting process";
	private static final String CHANGE_ACTION_MESSAGE = "Put index of phone item, which you want to change: ";
	private static final String CHANGE_ACTION_COMPLITE_MESSAGE = "All changes saved";
	private static final String CURRENT_PHONE_BOOK_ITEM_STATE = "Current item state";
	private static final String CHOOSE_ITEM_ERROR_MESSAGE = "Please, put right menu item";
	private static final String CONFIRM_REMOVE_KEY = "y";
	private static final String NO_SUCH_ITEM_IN_MENU_MESSAGE = "There is no such item in menu, please, try again";
	private static final String EXIT_MESSAGE = "Application was closed";
	private static final String NEW_LINE = "\n";

	// Object of class that provide treatment of user action with phone book
	PhoneBookTreatment phoneBookTreatment;

	/**
	 * Constructor.
	 */
	Conversation(final int parserId) {
		if (parserId == 1) {
			this.phoneBookTreatment = new PhoneBookDOMTreatment();
		} else if (parserId == 2) {
			this.phoneBookTreatment = new PhoneBookSAXTreatment();
		}
	}

	public static int chooseParserType() {
		ConsoleOperations.printInfo("Please, choose type of parser that you want to use");
		ConsoleOperations.printInfo("1. DOM");
		ConsoleOperations.printInfo("2. SAX");
		int userAnswer = ConsoleOperations.readNumber();
		while ((userAnswer != 1) && (userAnswer != 2)) {
			ConsoleOperations.printInfo("Try to choose right menu item");
			userAnswer = ConsoleOperations.readNumber();
		}

		if (userAnswer == 1) {
			ConsoleOperations.printInfo("Dom parser was loaded");
		} else {
			ConsoleOperations.printInfo("Sax parser was loaded");
		}
		return userAnswer;
	}

	/**
	 * Method provide treatment of "add info" action in main menu. This method
	 * read all needed information about phone book item (name, lastname,
	 * adress, number) and after that call method from
	 * {@link PhoneBookDOMTreatment} for create new element in our XML document
	 * (this file is used for storing all information in our application).
	 */
	public void addItemTreatment() {
		// Reading info for phone book item
		ConsoleOperations.printInfo(Conversation.ITEM_PUT_NAME_MESSAGE);
		final String name = ConsoleOperations.readInfo();
		// Read last name
		ConsoleOperations.printInfo(Conversation.ITEM_PUT_LASTNAME_MESSAGE);
		final String lastname = ConsoleOperations.readInfo();
		// Read adress
		ConsoleOperations.printInfo(Conversation.ITEM_PUT_ADRESS_MESSAGE);
		final String adress = ConsoleOperations.readInfo();
		// Read phone number
		ConsoleOperations.printInfo(Conversation.ITEM_PUT_NUMBER_MESSAGE);
		final String number = ConsoleOperations.readPhoneNumber();

		// Call method which can add our information in xml file
		this.phoneBookTreatment.addPhoneItem(name, lastname, adress, number);
	}

	/**
	 * Method used for printing main menu, which provide conversation with user.
	 */
	public void printMainPhoneBookInfo() {
		ConsoleOperations.printInfo(Conversation.DISPLAY_ITEMS_MENU_MESSAGE + Conversation.ADD_ITEM_MENU_MESSAGE
				+ Conversation.CHANGE_ITEM_MENU_MESSAGE + Conversation.REMOVE_ITEM_MENU_MESSAGE
				+ Conversation.SEARCH_ITEM_MENU_MESSAGE + Conversation.PLUG_XML_FILE_MENU_MESSAGE
				+ Conversation.CHANGE_PARSER_MENU_MESSAGE + Conversation.EXIT_ITEM_MENU_MESSAGE);
	}

	/**
	 * Method provide treatment of "remove info" action in main menu. Method ask
	 * user index of phone book item, which he want to remove, after that
	 * request to confirm deliting of this item. Before deleting we can see all
	 * information about this phone book item.
	 */
	public void removeItemTreatment() {
		ConsoleOperations.printInfo(Conversation.REMOVE_ITEM_MESSAGE);

		// Read index of item which user want to remove
		final int index = ConsoleOperations.readNumber();
		// Getting phone book item witn current index (which we read from
		// console)
		final PhoneBookItem phoneBookItem = this.phoneBookTreatment.getPhoneItemValue(index);

		if (phoneBookItem != null) {
			// Printing info about current phone book item
			ConsoleOperations.printInfo(phoneBookItem.getItemInfo());
			ConsoleOperations.printInfo(Conversation.NEW_LINE + Conversation.CONFIRM_ACTION_MESSAGE);
			// Reading confirm action
			final String answer = ConsoleOperations.readInfo();
			if (answer.equalsIgnoreCase(Conversation.CONFIRM_REMOVE_KEY)) {
				this.phoneBookTreatment.removePhoneItem(index);
			} else {
				ConsoleOperations.printInfo(Conversation.REMOVE_EXIT_MESSAGE);
			}
		}
	}

	/**
	 * Method used for treatment user input in main application menu.
	 *
	 * @param menuNumberValue
	 *            String value.
	 */
	public void selectionTreatmentForMainPhoneBookMenu(final int menuNumberValue) {
		switch (menuNumberValue) {
		case DISPLAY_ITEMS_MENU_VALUE:
			this.phoneBookTreatment.getAllPhoneItems();
			break;
		case ADD_ITEM_MENU_VALUE:
			this.addItemTreatment();
			break;
		case CHANGE_ITEM_MENU_VALUE:
			this.changeItemContentTreatment();
			break;
		case REMOVE_ITEM_MENU_VALUE:
			this.removeItemTreatment();
			break;
		case SEARCH_ITEM_MENU_VALUE:
			this.searchItemTreatment();
			break;
		case PLUG_XML_FILE_MENU_VALUE:
			this.changeXMLFile();
			break;
		case CHANGE_PARSER_MENU_VALUE:
			this.changeParserTreatment(Conversation.chooseParserType());
			break;
		case EXIT_ITEM_MENU_VALUE:
			ConsoleOperations.closeStream();
			ConsoleOperations.printInfo(Conversation.EXIT_MESSAGE);
			break;
		default:
			ConsoleOperations.printInfo(Conversation.NO_SUCH_ITEM_IN_MENU_MESSAGE + Conversation.NEW_LINE);
			break;
		}
	}

	/**
	 * Method used for change information in some phone book item.
	 */
	private void changeItemContentTreatment() {
		ConsoleOperations.printInfo(Conversation.CHANGE_ACTION_MESSAGE);
		final int index = ConsoleOperations.readNumber();
		final PhoneBookItem phoneBookItem = this.phoneBookTreatment.getPhoneItemValue(index);
		if (phoneBookItem != null) {
			int changeMenuItemValue = 0;
			while ((changeMenuItemValue != Conversation.CLOSE_CHANGES_DIALOG_KEY)
					&& (changeMenuItemValue != Conversation.RETURN_TO_MAIN_MENU_VALUE)) {
				ConsoleOperations.printInfo(
						Conversation.NEW_LINE + Conversation.CURRENT_PHONE_BOOK_ITEM_STATE + Conversation.NEW_LINE);
				ConsoleOperations.printInfo(phoneBookItem.getItemInfo());

				this.printChangePhoneBookItemInfo();
				changeMenuItemValue = ConsoleOperations.readNumber();
				this.changeItemSelectionTreatment(phoneBookItem, changeMenuItemValue);
			}
			this.phoneBookTreatment.changeItemState(index, phoneBookItem);
		}
	}

	/**
	 * Method used for change information in current object of
	 * {@link PhoneBookItem}. User but index of changes (1 - change name, 2 -
	 * change lastname, 3 - change adress, 4 - change phone number).
	 *
	 * @param item
	 *            {@link PhoneBookItem} value.
	 * @param menuIndex
	 *            int value.
	 */
	private void changeItemSelectionTreatment(final PhoneBookItem item, final int menuIndex) {
		switch (menuIndex) {
		case CHANGE_ITEM_NAME_VALUE:
			ConsoleOperations.printInfo(Conversation.ITEM_PUT_NAME_MESSAGE);
			item.setName(ConsoleOperations.readInfo());
			break;
		case CHANGE_ITEM_LASTNAME_VALUE:
			ConsoleOperations.printInfo(Conversation.ITEM_PUT_LASTNAME_MESSAGE);
			item.setLastname(ConsoleOperations.readInfo());
			break;
		case CHANGE_ITEM_ADRESS_VALUE:
			ConsoleOperations.printInfo(Conversation.ITEM_PUT_ADRESS_MESSAGE);
			item.setAdress(ConsoleOperations.readInfo());
			break;
		case CHANGE_ITEM_NUMBER_VALUE:
			ConsoleOperations.printInfo(Conversation.ITEM_PUT_NUMBER_MESSAGE);
			item.setNumber(ConsoleOperations.readPhoneNumber());
			break;
		case CHANGE_ITEM_FINISH_VALUE:
			ConsoleOperations.printInfo(Conversation.CHANGE_ACTION_COMPLITE_MESSAGE + Conversation.NEW_LINE);
			break;
		default:
			ConsoleOperations.printInfo(Conversation.CHOOSE_ITEM_ERROR_MESSAGE + Conversation.NEW_LINE);
			break;
		}
	}

	/**
	 *
	 * @param parserId
	 */
	private void changeParserTreatment(final int parserId) {
		if (parserId == 1) {
			this.phoneBookTreatment = new PhoneBookDOMTreatment();
		} else if (parserId == 2) {
			this.phoneBookTreatment = new PhoneBookSAXTreatment();
		}
	}

	/**
	 * Method used for change source of data. We can call external xml file for
	 * our application.
	 */
	private void changeXMLFile() {
		ConsoleOperations.printInfo("Put path to other xml file with information");
		final String filePath = ConsoleOperations.readInfo();
		final File xmlFile = new File(filePath);
		// Check is file exist and validate xml with dtd file
		if (xmlFile.exists() && XMLTreatment.isXMLFileFormat(filePath)) {
			boolean isValid = false;
			if (this.phoneBookTreatment instanceof PhoneBookDOMTreatment) {
				isValid = XMLTreatment.isExternalDTDValid(filePath, this.phoneBookTreatment.getValidatorFilePath());
			} else if (this.phoneBookTreatment instanceof PhoneBookSAXTreatment) {
				isValid = XMLTreatment.isExternalXSDValid(filePath, this.phoneBookTreatment.getValidatorFilePath());
			}
			if (isValid) {
				this.phoneBookTreatment.setSourceFilePath(filePath);
				ConsoleOperations.printInfo("File succesfully used");
			} else {
				ConsoleOperations.printInfo("Problem with validation");
			}
		} else {
			ConsoleOperations.printInfo("There is no such file");
		}
	}

	/**
	 * Method for printing header in "change info" action.
	 */
	private void printChangePhoneBookItemInfo() {
		ConsoleOperations.printInfo(Conversation.NEW_LINE + Conversation.CHANGE_ITEM_NAME_MESSAGE
				+ Conversation.CHANGE_ITEM_LASTNAME_MESSAGE + Conversation.CHANGE_ITEM_ADRESS_MESSAGE
				+ Conversation.CHANGE_ITEM_NUMBER_MESSAGE + Conversation.CHANGE_ITEM_FINISH_EDITING
				+ Conversation.RETURN_TO_MAIN_MENU);
	}

	/**
	 * Method for treatment "search" action.
	 */
	private void searchItemTreatment() {
		ConsoleOperations.printInfo("Put search key");
		final String searchKey = ConsoleOperations.readInfo();
		this.phoneBookTreatment.searchPhoneItem(searchKey);
	}
}

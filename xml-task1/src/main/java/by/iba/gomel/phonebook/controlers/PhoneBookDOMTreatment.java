/**
 *
 */
package by.iba.gomel.phonebook.controlers;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import by.iba.gomel.phonebook.console.ConsoleOperations;
import by.iba.gomel.phonebook.handlers.dom.ValidateErrorHandler;
import by.iba.gomel.phonebook.models.PhoneBookItem;

/**
 * Class used for provide application work with xml sources.
 */
public final class PhoneBookDOMTreatment implements PhoneBookTreatment {

	/**
	 * Logger.
	 *
	 * For printing information we used Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(PhoneBookItem.class);

	private String sourceFilePath;
	private final String dtdFilePath;
	private File xmlFile;
	private DocumentBuilderFactory factory;
	private DocumentBuilder builder;
	private Document document;

	/**
	 * Constructor.
	 */
	public PhoneBookDOMTreatment() {
		// Load file path of default xml sources
		final ClassLoader classLoader = this.getClass().getClassLoader();
		this.sourceFilePath = Constants.DEFAULT_XML_PHONE_BOOK_FILE_PATH;
		this.xmlFile = new File(classLoader.getResource(Constants.DEFAULT_XML_PHONE_BOOK_FILE_PATH).getFile());
		// Load file path of default dtd sources
		this.dtdFilePath = classLoader.getResource(Constants.DEFAULT_DTD_PHONE_BOOK_FILE_PATH).getPath();

		this.factory = DocumentBuilderFactory.newInstance();
		this.factory.setValidating(true);
		this.builder = null;

		try {
			this.builder = this.factory.newDocumentBuilder();
			this.builder.setErrorHandler(new ValidateErrorHandler());
			this.document = this.builder.parse(this.xmlFile);
			this.document.getDocumentElement().normalize();
		} catch (final IOException | ParserConfigurationException | SAXException e) {
			PhoneBookDOMTreatment.LOGGER.error("", e);
		}

	}

	/**
	 * Method for adding item in phone book (xml file).
	 *
	 * @param name
	 *            String value.
	 * @param lastname
	 *            String value.
	 * @param adress
	 *            String value.
	 * @param number
	 *            String value.
	 */
	@Override
	public void addPhoneItem(final String name, final String lastname, final String adress, final String number) {

		final Element phoneBookItem = this.document.getDocumentElement();
		final Element item = this.document.createElement(Constants.TAG_ITEM);

		item.setAttribute("starred", "no");

		// Fill <item> tag with <name>, <lastname>, <adress>, <number>
		this.addTagForNewElement(item, Constants.TAG_NAME, name);
		this.addTagForNewElement(item, Constants.TAG_LASTNAME, lastname);
		this.addTagForNewElement(item, Constants.TAG_ADRESS, adress);
		this.addTagForNewElementWithAttribute(item, Constants.TAG_PHONE_NUMBER, number, "is_viber", "no");

		phoneBookItem.appendChild(item);

		this.saveFileState();

	}

	/**
	 * Method for adding new tag in xml element.
	 *
	 * @param element
	 *            {@link Element} value.
	 * @param tagName
	 *            String value.
	 * @param tagValue
	 *            String value.
	 */
	public void addTagForNewElement(final Element element, final String tagName, final String tagValue) {
		final Element newTagelement = this.document.createElement(tagName);
		newTagelement.setTextContent(tagValue);
		element.appendChild(newTagelement);
	}

	/**
	 * Method for adding new tag with attribute in xml element.
	 *
	 * @param element
	 *            {@link Element}
	 * @param tagName
	 *            String value.
	 * @param tagValue
	 *            String value.
	 * @param attributeName
	 *            String value.
	 * @param attributeValue
	 *            String value.
	 */
	public void addTagForNewElementWithAttribute(final Element element, final String tagName, final String tagValue,
			final String attributeName, final String attributeValue) {
		final Element newTagelement = this.document.createElement(tagName);
		newTagelement.setAttribute(attributeName, attributeValue);
		newTagelement.setTextContent(tagValue);
		element.appendChild(newTagelement);
	}

	/**
	 * Method for change info in xml file about some phone book item.
	 *
	 * @param index
	 *            int value (index of item in phone book).
	 * @param item
	 *            Object of item (contains info about all field of phone book
	 *            item).
	 */
	@Override
	public void changeItemState(final int index, final PhoneBookItem item) {
		if (index > 0) {

			final NodeList phoneBookItems = this.document.getElementsByTagName(Constants.TAG_ITEM);
			if (index <= phoneBookItems.getLength()) {
				final Element element = (Element) phoneBookItems.item(index - 1);
				element.getElementsByTagName(Constants.TAG_NAME).item(0).setTextContent(item.getName());
				element.getElementsByTagName(Constants.TAG_LASTNAME).item(0).setTextContent(item.getLastname());
				element.getElementsByTagName(Constants.TAG_ADRESS).item(0).setTextContent(item.getAdress());
				element.getElementsByTagName(Constants.TAG_PHONE_NUMBER).item(0).setTextContent(item.getNumber());

				this.saveFileState();

			} else {
				ConsoleOperations.printInfo(Constants.PHONE_BOOK_ITEM_INDEX_ERROR_MESSAGE + Constants.NEW_LINE);
			}
		} else {
			ConsoleOperations.printInfo(Constants.PHONE_BOOK_NEGATIVE_ITEM_INDEX_ERROR_MESSAGE + Constants.NEW_LINE);
		}
	}

	/**
	 * Method used for display all information from xml file.
	 */
	@Override
	public void getAllPhoneItems() {

		Element item;

		final NodeList items = this.document.getElementsByTagName(Constants.TAG_ITEM);

		for (int i = 0; i < items.getLength(); i++) {
			item = (Element) items.item(i);
			ConsoleOperations.printInfo("№ " + (i + 1) + " "
					+ new PhoneBookItem(item.getElementsByTagName(Constants.TAG_NAME).item(0).getTextContent(),
							item.getElementsByTagName(Constants.TAG_LASTNAME).item(0).getTextContent(),
							item.getElementsByTagName(Constants.TAG_ADRESS).item(0).getTextContent(),
							item.getElementsByTagName(Constants.TAG_PHONE_NUMBER).item(0).getTextContent())
									.getItemInfo());

		}

		ConsoleOperations.printInfo(Constants.NEW_LINE);
	}

	/**
	 * Method for getting amount of phone book items in items in xml file.
	 *
	 * @return
	 */
	@Override
	public int getAmountOfItems() {
		int result;
		final NodeList items = this.document.getElementsByTagName(Constants.TAG_ITEM);
		result = items.getLength();
		return result;
	}

	/**
	 * Method for getting current dtd file path.
	 *
	 * @return String value.
	 */
	public String getDtdFilePath() {
		return this.dtdFilePath;
	}

	/**
	 * Method for getting info about some phone book item by index.
	 *
	 * @param index
	 *            index of item.
	 * @return PhoneBookItem object with all info about item.
	 */
	@Override
	public PhoneBookItem getPhoneItemValue(final int index) {
		PhoneBookItem phoneBookItem = null;
		if (index > 0) {
			final NodeList phoneBookItems = this.document.getElementsByTagName(Constants.TAG_ITEM);
			if (index <= phoneBookItems.getLength()) {
				final Element element = (Element) phoneBookItems.item(index - 1);
				phoneBookItem = new PhoneBookItem(
						element.getElementsByTagName(Constants.TAG_NAME).item(0).getTextContent(),
						element.getElementsByTagName(Constants.TAG_LASTNAME).item(0).getTextContent(),
						element.getElementsByTagName(Constants.TAG_ADRESS).item(0).getTextContent(),
						element.getElementsByTagName(Constants.TAG_PHONE_NUMBER).item(0).getTextContent());

				this.saveFileState();

			} else {
				ConsoleOperations.printInfo(Constants.PHONE_BOOK_ITEM_INDEX_ERROR_MESSAGE);
			}
		} else {
			ConsoleOperations.printInfo(Constants.PHONE_BOOK_NEGATIVE_ITEM_INDEX_ERROR_MESSAGE);
		}
		return phoneBookItem;
	}

	@Override
	public String getValidatorFilePath() {
		return this.getDtdFilePath();
	}

	/**
	 * Method for removing "item" from xml file.
	 *
	 * @param index
	 *            index of item that we wants to delete.
	 */
	@Override
	public void removePhoneItem(final int index) {
		if (index > 0) {
			final NodeList phoneBookItems = this.document.getElementsByTagName(Constants.TAG_ITEM);
			if (index <= phoneBookItems.getLength()) {
				final Element element = (Element) phoneBookItems.item(index - 1);
				this.document.getDocumentElement().removeChild(element);

				this.saveFileState();
			} else {
				ConsoleOperations.printInfo(Constants.PHONE_BOOK_ITEM_INDEX_ERROR_MESSAGE);
			}
		} else {
			ConsoleOperations.printInfo(Constants.PHONE_BOOK_NEGATIVE_ITEM_INDEX_ERROR_MESSAGE);
		}
	}

	/**
	 * Method for saving changes in xml file.
	 *
	 * @throws TransformerException
	 *             Error.
	 */
	@Override
	public void saveFileState() {
		final TransformerFactory transformerFactory = TransformerFactory.newInstance();
		try {
			Transformer transformer;
			transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			final DOMImplementation domImpl = this.document.getImplementation();
			final DocumentType doctype = domImpl.createDocumentType("doctype", "SYSTEM", "phone_items.dtd");
			transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, doctype.getPublicId());
			transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, doctype.getSystemId());
			final DOMSource source = new DOMSource(this.document);
			final StreamResult result = new StreamResult(this.xmlFile);
			transformer.transform(source, result);
		} catch (final TransformerException e) {
			PhoneBookDOMTreatment.LOGGER.error("", e);
		}
	}

	/**
	 * Method for search item by some key(name, lastname and ect.) and value
	 *
	 * @param tagName
	 *            String value.
	 * @param searchValue
	 *            String value.
	 */
	@Override
	public void searchPhoneItem(final String searchValue) {
		final NodeList items = this.document.getElementsByTagName(Constants.TAG_ITEM);

		for (int i = 0; i < items.getLength(); i++) {
			final Element item = (Element) items.item(i);
			if (item.getElementsByTagName("name").item(0).getTextContent().equals(searchValue)
					|| item.getElementsByTagName("lastname").item(0).getTextContent().equals(searchValue)
					|| item.getElementsByTagName("number").item(0).getTextContent().equals(searchValue)) {
				ConsoleOperations.printInfo(
						new PhoneBookItem(item.getElementsByTagName(Constants.TAG_NAME).item(0).getTextContent(),
								item.getElementsByTagName(Constants.TAG_LASTNAME).item(0).getTextContent(),
								item.getElementsByTagName(Constants.TAG_ADRESS).item(0).getTextContent(),
								item.getElementsByTagName(Constants.TAG_PHONE_NUMBER).item(0).getTextContent())
										.getItemInfo());

			}
		}
	}

	@Override
	public void setSourceFilePath(final String filePath) {
		this.sourceFilePath = filePath;
		this.xmlFile = new File(this.sourceFilePath);
		// Load file path of default dtd sources

		this.factory = DocumentBuilderFactory.newInstance();
		this.factory.setValidating(true);
		this.builder = null;

		try {
			this.builder = this.factory.newDocumentBuilder();
			this.builder.setErrorHandler(new ValidateErrorHandler());
			this.document = this.builder.parse(this.xmlFile);
			this.document.getDocumentElement().normalize();
		} catch (final IOException | ParserConfigurationException | SAXException e) {
			PhoneBookDOMTreatment.LOGGER.error("", e);
		}
	}

}

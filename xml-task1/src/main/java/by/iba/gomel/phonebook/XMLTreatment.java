/**
 *
 */
package by.iba.gomel.phonebook;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.regex.Pattern;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import by.iba.gomel.phonebook.handlers.DefaultErrorHandler;
import by.iba.gomel.phonebook.handlers.dom.ValidateErrorHandler;

/**
 * Class used for treatment xml files. Check format of file (is file have xml
 * format). Also we can validate xml-file with external dtd file.
 */
public final class XMLTreatment {

	/**
	 * Logger.
	 *
	 * For printing information we used Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(XMLTreatment.class);

	/**
	 * Constructor.
	 */
	XMLTreatment() {
		// Empty constructor.
	}

	/**
	 * Method for check format.
	 *
	 * @param fileName
	 *            String value.
	 * @return boolean value.
	 */
	public static boolean isDTDFileFormat(final String fileName) {
		return Pattern.compile("((.){0,}[.](dtd)$){0,}").matcher(fileName.toLowerCase()).matches();
	}

	/**
	 * Method for check validating of file with external dtd file.
	 *
	 * @param xmlFileName
	 *            String value.
	 * @param dtdFileName
	 *            String value.
	 * @return
	 */
	public static boolean isExternalDTDValid(final String xmlFileName, final String dtdFileName) {
		// Check format for xml and dtd files
		if (XMLTreatment.isXMLFileFormat(xmlFileName) && XMLTreatment.isDTDFileFormat(dtdFileName)) {
			final ValidateErrorHandler validateErrorHandler = new ValidateErrorHandler();
			final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(true);
			DocumentBuilder builder;

			try {
				builder = factory.newDocumentBuilder();
				builder.setErrorHandler(new DefaultErrorHandler());
				final Document doc = builder.parse(new File(xmlFileName));
				final DOMSource source = new DOMSource(doc);

				// Use a transformer to add the DTD element
				final TransformerFactory tf = TransformerFactory.newInstance();
				final Transformer transformer = tf.newTransformer();
				transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, dtdFileName);
				final StringWriter writer = new StringWriter();
				final StreamResult result = new StreamResult(writer);
				transformer.transform(source, result);

				// Finally parse the result
				builder.setErrorHandler(validateErrorHandler);
				builder.parse(new InputSource(new StringReader(writer.toString())));

			} catch (final ParserConfigurationException | SAXException | IOException | TransformerException e) {
				XMLTreatment.LOGGER.error("", e);
			}
			// If xml contains some error validateHandler will tell about this.
			return validateErrorHandler.isFileValid();
		}
		return false;
	}

	/**
	 * Method for check validating of file with external xsd file.
	 *
	 * @param xmlFileName
	 *            String value.
	 * @param xsdFileName
	 *            String value.
	 * @return boolean value.
	 */
	public static boolean isExternalXSDValid(final String xmlFileName, final String xsdFileName) {
		final ValidateErrorHandler validateErrorHandler = new ValidateErrorHandler();
		try {
			final SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			final Schema schema = factory.newSchema(new StreamSource(xsdFileName));
			final Validator validator = schema.newValidator();

			validator.setErrorHandler(validateErrorHandler);
			validator.validate(new StreamSource(xmlFileName));
		} catch (final Exception e) {
			XMLTreatment.LOGGER.error("", e);
		}
		return validateErrorHandler.isFileValid();

	}

	/**
	 * Method for check validating of file with external xsd file.
	 *
	 * @param xmlFileName
	 *            String value.
	 * @param xsdFileName
	 *            String value.
	 * @return boolean value.
	 */
	public static boolean isExternalFileValid(final String xmlFileName, final String schemaFile) {
		return XMLTreatment.isExternalDTDValid(xmlFileName, schemaFile)
				|| XMLTreatment.isExternalXSDValid(xmlFileName, schemaFile);
	}

	/**
	 * Check file format method.
	 *
	 * @param fileName
	 *            String value.
	 * @return boolean value.
	 */
	public static boolean isXMLFileFormat(final String fileName) {
		return Pattern.compile("((.){0,}[.](xml)$){0,}").matcher(fileName.toLowerCase()).matches();
	}
}

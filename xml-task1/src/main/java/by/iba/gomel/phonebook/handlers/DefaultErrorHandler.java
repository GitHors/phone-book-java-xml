/**
 *
 */
package by.iba.gomel.phonebook.handlers;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * Default empty handler.
 */
public class DefaultErrorHandler implements ErrorHandler {

	@Override
	public void error(final SAXParseException exception) throws SAXException {
		//
	}

	@Override
	public void fatalError(final SAXParseException exception) throws SAXException {
		//
	}

	@Override
	public void warning(final SAXParseException exception) throws SAXException {
		//
	}

}

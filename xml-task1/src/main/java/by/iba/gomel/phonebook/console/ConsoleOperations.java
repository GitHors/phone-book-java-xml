/**
 *
 */
package by.iba.gomel.phonebook.console;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class used for provide work in console (reading, printing into console).
 */
public final class ConsoleOperations {

	// Messages about errors
	private static final String IO_ERROR = "Input/Output Error";
	private static final String NUMBER_IO_ERROR = "You must put number value at this step";

	static final Charset charset = Charset.forName("US-ASCII");
	static BufferedReader bufferedReader = new BufferedReader(
			new InputStreamReader(System.in, ConsoleOperations.charset));

	/**
	 * Logger.
	 *
	 * For printing information we used Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(ConsoleOperations.class);

	/**
	 * Constructor.
	 */
	ConsoleOperations() {

	}

	/**
	 * Method used for close console reading.
	 */
	public static void closeStream() {
		try {
			ConsoleOperations.bufferedReader.close();
		} catch (final IOException e) {
			ConsoleOperations.LOGGER.error("Error", e);
		}
	}

	/**
	 * Method used for printing text in console.
	 *
	 * @param info
	 *            String value.
	 */
	public static void printInfo(final String info) {
		ConsoleOperations.LOGGER.info(info);
	}

	/**
	 * Method used for scan some information form console.
	 *
	 * @return result of reading (String value).
	 * @throws IOException
	 *             Error.
	 */
	public static String readInfo() {
		String result = "";

		try {
			result = ConsoleOperations.bufferedReader.readLine();
		} catch (final IOException ioe) {
			ConsoleOperations.LOGGER.error(ConsoleOperations.IO_ERROR, ioe);
		}
		return result;
	}

	/**
	 * Method used for reading numbers from console.
	 *
	 * @return Int value.
	 */
	public static int readNumber() {
		String result = "";
		int menuItem = 0;
		try {
			result = ConsoleOperations.bufferedReader.readLine();
			menuItem = Integer.parseInt(result);
		} catch (final IOException ioe) {
			ConsoleOperations.LOGGER.error(ConsoleOperations.IO_ERROR, ioe);
		} catch (final NumberFormatException nfe) {
			ConsoleOperations.LOGGER.info(ConsoleOperations.NUMBER_IO_ERROR);
			menuItem = ConsoleOperations.readNumber();
		}

		return menuItem;
	}

	/**
	 * Method used for reading and validate phone number. For example (+3785654,
	 * +375-29-369-654, 208056)
	 *
	 * @return String value.
	 */
	public static String readPhoneNumber() {
		String phoneNumber = "";
		try {
			phoneNumber = ConsoleOperations.bufferedReader.readLine();
			while (!Pattern.compile("(([+]){0,1}(([0-9]){1,}([-]){0,1}){0,}){1}").matcher(phoneNumber).matches()) {
				ConsoleOperations.printInfo("Try again");
				phoneNumber = ConsoleOperations.bufferedReader.readLine();
			}
		} catch (final IOException e) {
			ConsoleOperations.LOGGER.error(ConsoleOperations.IO_ERROR, e);
		}
		return phoneNumber;
	}

}

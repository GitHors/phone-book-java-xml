
/**
 *
 */
package by.iba.gomel.phonebook;

import by.iba.gomel.phonebook.console.ConsoleOperations;

/**
 * Class used for staring our phone book application, which provide conversation
 * with user by console.
 */
public class Runner {

	/**
	 * Constructor.
	 */
	Runner() {

	}

	/**
	 * Main method.
	 *
	 * @param args
	 *            String value.
	 */
	public static void main(final String[] args) {
		final int parserId = Conversation.chooseParserType();
		final Conversation conversation = new Conversation(parserId);

		int menuNumber = 0;

		while (menuNumber != Conversation.EXIT_ITEM_MENU_VALUE) {
			conversation.printMainPhoneBookInfo();
			menuNumber = ConsoleOperations.readNumber();
			conversation.selectionTreatmentForMainPhoneBookMenu(menuNumber);
		}
	}

}

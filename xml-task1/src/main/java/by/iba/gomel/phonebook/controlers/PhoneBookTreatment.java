package by.iba.gomel.phonebook.controlers;

import by.iba.gomel.phonebook.models.PhoneBookItem;

public interface PhoneBookTreatment {

	/**
	 * Method for adding phone book item.
	 *
	 * @param name
	 * @param lastname
	 * @param adress
	 * @param number
	 */
	void addPhoneItem(String name, String lastname, String adress, String number);

	/**
	 * Method for changing phone book item.
	 *
	 * @param index
	 * @param item
	 */
	void changeItemState(int index, PhoneBookItem item);

	/**
	 * Method for getting all phone book items.
	 */
	void getAllPhoneItems();

	/**
	 * Method for getting phone book items amount.
	 *
	 * @return
	 */
	int getAmountOfItems();

	/**
	 * Method for getting phone book item.
	 *
	 * @param index
	 * @return
	 */
	PhoneBookItem getPhoneItemValue(int index);

	/**
	 * Method for getting file path of xsd or dtd.
	 *
	 * @return
	 */
	String getValidatorFilePath();

	/**
	 * Method for remove phone book item.
	 *
	 * @param index
	 */
	void removePhoneItem(int index);

	/**
	 * Method for save changes in file.
	 */
	default void saveFileState() {
	}

	/**
	 * Method for searcing phone book item.
	 *
	 * @param searchValue
	 */
	void searchPhoneItem(final String searchValue);

	void setSourceFilePath(String filePath);
}
